//
//  CITestApp.swift
//  CITest
//
//  Created by huluobo on 2021/8/6.
//

import SwiftUI

@main
struct CITestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
