//
//  Util.swift
//  CITest
//
//  Created by huluobo on 2021/8/6.
//

import Foundation

public struct Util {
    static func hello(_ name: String?) -> String {
        guard let name = name else {
            return "hello"
        }
        return "hello, \(name)"
    }
}
