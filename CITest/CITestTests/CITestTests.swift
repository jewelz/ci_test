//
//  CITestTests.swift
//  CITestTests
//
//  Created by huluobo on 2021/8/6.
//

import XCTest
@testable import CITest

class CITestTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        let ret = Util.hello(nil)
        XCTAssertTrue(ret == "hello", "name should't be nil")
    }
    
    func testHello() throws {
        XCTAssertTrue(Util.hello("jewelz") == "hello, jewelz")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
